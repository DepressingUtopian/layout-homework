function switchToFloat() {
    document.getElementById('stylesheet').href='styles_float.css';
}

function switchToInlineBlock() {
    document.getElementById('stylesheet').href='styles_inline-block.css';
}

function switchToFlex() {
    document.getElementById('stylesheet').href='styles_flex.css';
}